DROP TABLE IF EXISTS press_process;

CREATE TABLE press_process (
	app_id varchar(255)  NOT NULL,
	num_presses mediumint NOT NULL
);