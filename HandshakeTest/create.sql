DROP TABLE IF EXISTS handshake;

CREATE TABLE identities (
	id mediumint NOT NULL AUTO_INCREMENT PRIMARY KEY,
	band_id varchar(255) NOT NULL,
	lastname varchar(255) NOT NULL,
	firstname varchar(255) NOT NULL,
	title varchar(255) NOT NULL,
	attendee_type mediumint NOT NULL
);

CREATE TABLE unmatched_connections (
	id mediumint NOT NULL AUTO_INCREMENT PRIMARY KEY,
	band_id varchar(255) NOT NULL,
	unique_id varchar(255) NOT NULL,
	connect_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE matched_connections (
	id mediumint NOT NULL AUTO_INCREMENT PRIMARY KEY,	
	band_id0 varchar(255) NOT NULL,
	band_id1 varchar(255) NOT NULL,
	connect_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);