<?php

// Helper method to get a string description for an HTTP status code
// From http://www.gen-x-design.com/archives/create-a-rest-api-with-php/ 
function getStatusCodeMessage($status)
{
    $codes = Array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported'
    );
 
    return (isset($codes[$status])) ? $codes[$status] : '';
}
 
// Helper method to send a HTTP response code/message
function sendResponse($status = 200, $body = '', $content_type = 'text/html')
{
    $status_header = 'HTTP/1.1 ' . $status . ' ' . getStatusCodeMessage($status);
    header($status_header);
    header('Content-type: ' . $content_type);
    echo $body;
}
 
class HandshakeAPI 
{
    private $db;
 
    // Constructor - open DB connection
    function __construct() 
    {
        $this->db = new mysqli('localhost', 'username', 'password', 'handshake');
        $this->db->autocommit(TRUE);
    }
 
    // Destructor - close DB connection
    function __destruct() 
    {
        $this->db->close();
    }

    function add_unmatched_connection() 
    {
	    // Check for required parameters
	    if (isset($_POST["band_id"]) && isset($_POST["unique_id"])) 
	    {
	        //Store posted band_id and unique_id into local variable
	        $my_band_id = $_POST["band_id"];
            $my_unique_id = $_POST["unique_id"];
	           
	        //Add band_id to unmatched connections
            $this->db->query("INSERT INTO unmatched_connections (band_id, unique_id, connect_time) VALUES ('$my_band_id', '$my_unique_id', DEFAULT)");

            //Search for newly created row id
            $stmt = $this->db->prepare("SELECT id FROM unmatched_connections WHERE band_id=? AND unique_id=?");
            $stmt->bind_param("ss", $my_band_id, $my_unique_id);
            $stmt->execute();
            $stmt->bind_result($my_id);
            while ($stmt->fetch()) 
            {
                break;
            }
            $stmt->close();

            //Data successfully stored
            if ($my_id > 0)
            {
    	        $result = array("result" => "OK");
    	        sendResponse(200, json_encode($result));
                return true;
            }
            //Data not successfully stored
            else
            {
                $result = array("result" => "BAD");
                sendResponse(500, json_encode($result));
                return false;
            }
	    }

        else if (isset($_POST["band_id"]) && isset($_POST["request_id"])) 
        {
            //Store posted band_id and request_id into local variable
            $my_band_id = $_POST["band_id"];
            $my_request_id = $_POST["request_id"];

            //Search for connect_time in unmatched connection with this band_id
            $stmt = $this->db->prepare("SELECT connect_time FROM unmatched_connections WHERE band_id=?");
            $stmt->bind_param("s", $my_band_id);
            $stmt->execute();
            $stmt->bind_result($my_connect_time);
            while ($stmt->fetch()) 
            {
                break;
            }
            $stmt->close();

            //Unmatched connection with this band_id found
            if (isset($my_connect_time))
            {   
                $timestamp = strtotime($my_connect_time); 
                $timestamp0 = $timestamp-2; 
                $timestamp1 = $timestamp+2;
                $my_connect_time_0 = date("Y-m-d H:i:s",$timestamp0); 
                $my_connect_time_1 = date("Y-m-d H:i:s",$timestamp1);
                //Search for connect_time in unmatched connection with this band_id
                //$stmt = $this->db->prepare("SELECT band_id FROM unmatched_connections WHERE (band_id<>? AND (connect_time BETWEEN DATEADD(ss,-2,?) AND DATEADD(ss,2,?)))");
                $stmt = $this->db->prepare("SELECT band_id FROM unmatched_connections WHERE (band_id<>? AND (connect_time BETWEEN ? AND ?))");
                $stmt->bind_param("sss", $my_band_id, $my_connect_time_0, $my_connect_time_1);
                $stmt->execute();
                $stmt->bind_result($their_band_id);
                while ($stmt->fetch()) 
                {
                    break;
                }
                $stmt->close();

                //If connection is found, store it in matched_connections table and remove it from unmatched_connections table
                if (isset($their_band_id))
                {
                    $this->db->query("INSERT INTO matched_connections (band_id0, band_id1, connect_time) VALUES ('$their_band_id', '$my_band_id', DEFAULT)");

                    //Lastname, firstname, title and attendee type
                    $stmt = $this->db->prepare("SELECT lastname, firstname, title, attendee_type, image_url FROM identities WHERE band_id=?");
                    $stmt->bind_param("s", $their_band_id);
                    $stmt->execute();
                    $stmt->bind_result($their_lastname, $their_firstname, $their_title, $their_attendee_type, $their_image_url);
                    while ($stmt->fetch()) 
                    {
                        break;
                    }
                    $stmt->close();


                    //$this->db->query("DELETE FROM unmatched_connections WHERE (band_id='$my_band_id' AND connect_time='$my_connect_time')");
                    $result = array("their_lastname" => $their_lastname, "their_firstname" => $their_firstname, "their_title" => $their_title, "their_attendee_type" => $their_attendee_type, "their_image_url" => $their_image_url);
                    sendResponse(200, json_encode(array("people"=>$result)));
                    return true;
                }
                else
                {
                    $this->db->query("DELETE FROM unmatched_connections WHERE (band_id='$my_band_id' AND connect_time='$my_connect_time')");
                    $result = array("result" => "NO_MATCH");
                    sendResponse(400, json_encode($result));
                    return false;
                }
            }
        }

	    sendResponse(400, 'Invalid request');
	    return false;
	}
}
 
$api = new HandshakeAPI;
$api->add_unmatched_connection();
 
?>