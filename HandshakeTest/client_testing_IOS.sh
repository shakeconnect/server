#!/bin/bash
chars=({a..z} {A..Z} {0..9})

function rand_string {
    local c=16 ret=
    while((c--)); do
        ret+=${chars[$((RANDOM%${#chars[@]}))]}
    done
    printf '%s\n' "$ret"
}

band_id_0="band_id=1"

unique_id="unique_id="
rand_0=$(rand_string)
unique_id_0=$unique_id$rand_0

server_url="shakeconnectserver.ddns.net/data/index.php"

request_update="request_id="
rand_0=$(rand_string)
request_id_0=$request_update$rand_0

curl -F $band_id_0 -F $unique_id_0 $server_url
sleep 2
curl -F $band_id_0 -F $request_id_0 $server_url
